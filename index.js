const vbManager = require('./vbManage/node-virtualbox/lib/virtualbox')
const express = require('express')
const multer = require('multer')
const path = require('path')
const myUtils = require('./myutils')

const app = express()

app.use(express.json())
app.use(multer({dest: "files"}).single('file'))

app.listen(8000, () => {
    console.log("Listening on 8000...")
})

app.post('/', async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const payload = req.body;
    // let cmd = payload.command
    
    if (payload.command === "login") {
        const token = myUtils.generateAccessToken(payload.username, payload.password)
        myUtils.responseJson(req, res, "Logged in", token)
    }

    // check for access admin or user1 -> cmd and vm accessibility
    let authbool = myUtils.authenticateToken(req, res)
    console.log(authbool);
    if (authbool) {
        var cmd = payload.command
    } else {
        myUtils.responseJson(req, res, "Not Authorized", "You can't make commands without authorization!")
    }

    if (cmd === "status") {
        await runStatusCmd(payload.vmName, req, res)
    } else if (cmd === "clone") {
        await runCloneCmd(payload.sourceVmName, payload.destVmName, req, res)
    } else if (cmd === "execute") {
        await runExecuteCmd(payload.vmName, payload.input, req, res)
    } else if (cmd === "on") {
        await runTurnOnCmd(payload.vmName, req, res)
    } else if (cmd === "off") {
        await runTurnOffCmd(payload.vmName, req, res)
    } else if (cmd === "modify") {
        await runModifyCmd(payload.vmName, payload.cpu, payload.ram, req, res)
    } else if (cmd === "delete") {
        await runDeleteCmd(payload.vmName, req, res)
    } else if (cmd === "upload") {
        await runUploadCmd(payload.vmName, payload.destPath, req.file.path, req.file.originalname, req, res)
        //fs.rmSync(req.file.path);
    } else if (cmd === "transfer") {
        await runTransferCmd(payload.sourceVmName, payload.sourcePath, payload.destVM, payload.destPath, req, res)
    }
})

const checkAccessibility = (username) => {

}

const isVmExist = (vmName, req, res) => {
    return new Promise((resolve, rej) => {
        vbManager.list(async (vms, err) => {
            let result = Object.values(vms).filter((vm) => vm.name === vmName);
            if (result.length !== 0) {
                resolve(result)
            } else {
                //rej(`There is no VM with ${vmName} name`)
                console.log(`There is no VM with ${vmName} name`);
                myUtils.responseJson(req, res, "Error", `Existence Error, ${vmName} not exist`)
            }
        })
    })
}

const getVmStatus = async (vmName) => {
    return new Promise((res, rej) => {
        vbManager.isRunning(vmName, async (err, out) => {
            if (await executeCommand(vmName, "ls") !== undefined) {
                if (out) {
                    res("on")
                } else {
                    res("powering off")
                }
            } else {
                if (out) {
                    res("powering on")
                } else {
                    res("off")
                }
            }
            if (err) {
                rej("error in isRunning!")
            }
        })
    });
}

const executeCommand = async (vmName, cmd) => { 
    return new Promise((res, rej) => {
        vbManager.exec({vm: vmName, cmd: cmd}, (err, out) => {
            if (err) {
                res(undefined)
            } else{
                res(out)
            }
        })
    })
}

const runStatusCmd = async (vmName, req, res) => {
    if (vmName) {
        await isVmExist(vmName, req, res)
        let status = await getVmStatus(vmName)
        myUtils.responseJson(req, res, status, "got status!")
    } else {
        vbManager.list(async (vms, err) => {
            let result = Object.values(vms).map(async (vm) => {
                return {
                    vmName: vm.name,
                    status: await getVmStatus(vm.name)
                }
            });
            myUtils.responseJson(req, res, "all Available VMs status", await Promise.all(result))
        })
    }
}

const runTurnOnCmd = async (vmName, req, res) => {
    await isVmExist(vmName, req, res)
    vbManager.start(vmName, false, (err) => {
        if (err) {
            myUtils.responseJson(req, res, "error", `Error in Turn On Command -> ${err}`)
        } else {
            myUtils.responseJson(req, res, "powering on", "Running Turn on CMD!")
        }
    })
}

const runTurnOffCmd = async (vmName, req, res) => {
    await isVmExist(vmName, req, res)
    vbManager.poweroff(vmName, (err) => {
        if (err) {
            myUtils.responseJson(req, res, "error", `Error in Turn Off Command -> ${err}`)
        } else {
            myUtils.responseJson(req, res, "powering off", "Running Turn OFF CMD!")
        }
    })
}

const runModifyCmd = async (vmName, cpu, ram, req, res) => {
    await isVmExist(vmName, req, res)
    let vmStatus = await getVmStatus(vmName)
    if (vmStatus === "off") {
        vbManager.modify(vmName, {memory: ram, cpus: cpu}, (err) => {
            if (err) {
                myUtils.responseJson(req, res, "error", `Error in ModifyCMD -> ${err}`)
            } else {
                myUtils.responseJson(req, res, "Ok", `Modifying ${vmName}`)
            }
        })
    } else {
        myUtils.responseJson(req, res, "Error", "Can't Modify because VM status muse be in OFF mode!")
    }
}

const runCloneCmd = async (sourceVmName, destVmName, req, res) => {
    await isVmExist(sourceVmName, req, res)
    vbManager.clone(sourceVmName, destVmName, (err) => {
        if (err) {
            myUtils.responseJson(req, res, "Error", `Error in CloneCMD -> ${err}`)
        } else {
            myUtils.responseJson(req, res, "Ok", `Cloning ${destVmName} from ${sourceVmName}`)
        }
    })
}

const runDeleteCmd = async (vmName, req, res) => {
    await isVmExist(vmName, req, res)
    vbManager.vmDelete(vmName, (err) => {
        if (err) {
            myUtils.responseJson(req, res, "Error", `Error in DeleteCMD -> ${err}`)
        } else {
            myUtils.responseJson(req, res, "Ok", `${vmName} is just Deleted!`)
        }
    })
}

const runExecuteCmd = async (vmName, input, req ,res) => {
    await isVmExist(vmName, req, res)
    let out = await executeCommand(vmName, input)
    if (out === undefined){
        myUtils.responseJson(req, res, "Error", `Error in ExecuteCMD vm ${vmName}`)
    } else {
        myUtils.responseJson(req, res, "Ok", `cmd executed, result: ${out}`)
    }
}

const runUploadCmd = async (vmName, destPath, sourcePath, originalname, req, res) => {
    await isVmExist(vmName)
    return new Promise((resolve, rej) => {
        const destinationPath = destPath + "/" + originalname
        vbManager.uploadTo(vmName, sourcePath, destinationPath, (err) => {
            if (err) {
                myUtils.responseJson(req, res, "error", `Error in UploadCMD -> ${err}`)
            } else {
                myUtils.responseJson(req, res, "Ok", `File ${originalname} Uploaded`)
                resolve()
            }
        })
    })
}

const runTransferCmd = async (sourceVmName, sourcePath, destVM, destPath, req, res) => {
    await isVmExist(sourceVmName)
    await isVmExist(destVM)
    const transFileName = path.basename(sourcePath)
    vbManager.downloadFrom(sourceVmName, sourcePath, "files\\"+transFileName, async (err) => {
        if (err) {
            myUtils.responseJson(req, res, "Error", `Error in TransferCMD in downloadFrom`)
        } else {
            await runUploadCmd(destVM, destPath, "files\\"+transFileName, transFileName, req, res)
        }
    })
}