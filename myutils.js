const jwt = require('jsonwebtoken')
const SECRET_TOKEN = "0751165d5f99e194f7a56f33cebac70b63facfc27524eacbeb7aac5f9a9e3a82"
users = ["admin", "user1"]
passwords = ["admin1", "user1"]

const generateAccessToken = (username, password) => {
    if (passwords[users.indexOf(username)] === password) {
        return jwt.sign(username, SECRET_TOKEN)
    } else {
        return false
    }
}

const authenticateToken = (req, res) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) {
        return false
    }
    let verifyBool = false
    jwt.verify(token, SECRET_TOKEN, (err, username) => {
        if (err) {
            verifyBool = false
        }
        req.username = username
        if (username === "user1") {
            let vmName = req.body.vmName
            let sourceVmName = req.body.sourceVmName
            // console.log((vmName !== undefined && vmName === "VM2") || (sourceVmName !== undefined && sourceVmName === "VM2"));
            if ((vmName !== undefined && vmName === "VM2") || (sourceVmName !== undefined && sourceVmName === "VM2")) {
                verifyBool = true
            } else {
                responseJson(req, res, "Failed", "user1 has only access to VM2")
                verifyBool = false
            }
        } else {
            verifyBool = true
        }
    })
    return verifyBool
}

const responseJson = (req, res, status, message) => {
    res.json({
        ...req.body, status: status, message: message
    })
    res.end();
}

module.exports = {
    generateAccessToken: generateAccessToken,
    authenticateToken: authenticateToken,
    responseJson: responseJson
};